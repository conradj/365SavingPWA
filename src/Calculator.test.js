import calculator from "./Calculator";

it("Get_Saving_Amount_For_1p_2_Day", () => {
  const fromDate = new Date("2018-01-01"); // 1p
  const toDate = new Date("2018-01-02"); // 2p
  const startAmount = 0;
  const incrementAmount = 1;

  expect(
    calculator.getSavingAmountForDate(
      fromDate,
      toDate,
      startAmount,
      incrementAmount
    )
  ).toEqual("0.03");
});

it("Get_Saving_Amount_For_1p_per_day_1_year", () => {
  const fromDate = new Date("2018-01-01"); // 1p
  const toDate = new Date("2018-12-31");
  const startAmount = 0;
  const incrementAmount = 1;

  expect(
    calculator.getSavingAmountForDate(
      fromDate,
      toDate,
      startAmount,
      incrementAmount
    )
  ).toEqual("667.95");
});

it("Get_Saving_Amount_For_1pound_per_week_1_year", () => {
  const fromDate = new Date("2018-01-01"); // 1p
  const toDate = new Date("2018-12-31");
  const startAmount = 0;
  const incrementAmount = 100;
  const incrementDays = 7;

  expect(
    calculator.getSavingAmountForDate(
      fromDate,
      toDate,
      startAmount,
      incrementAmount,
      incrementDays
    )
  ).toEqual("1378.00");
});

it("Get_Saving_Amount_For_reverse_1p_1_year", () => {
  const fromDate = new Date("2018-01-01"); // 1p
  const toDate = new Date("2018-12-31");
  const startAmount = 3.65;
  const incrementAmount = -1;

  expect(
    calculator.getSavingAmountForDate(
      fromDate,
      toDate,
      startAmount,
      incrementAmount
    )
  ).toEqual("667.95");
});

it("Get_Todays_Deposit_For_Day_1_1p_1_year", () => {
  expect(
    calculator.getTodaysDeposit(
      new Date("2018-01-01"),
      new Date("2018-01-01"),
      0.01,
      1,
      1
    )
  ).toEqual("0.01");
});

it("Get_Todays_Deposit_For_Day_365_1p_1_year", () => {
  const fromDate = new Date("2018-01-01"); // 1p
  const toDate = new Date("2018-12-31");
  const startAmount = 0.01;
  const incrementAmount = 1;

  expect(
    calculator.getTodaysDeposit(
      fromDate,
      toDate,
      startAmount,
      incrementAmount,
      1
    )
  ).toEqual("3.65");
});

it("Get_Todays_Deposit_For_Day_365_1p_over_1_year", () => {
  expect(
    calculator.getTodaysDeposit(
      new Date("2018-01-01"),
      new Date("2019-01-01"),
      0.01,
      1,
      1
    )
  ).toEqual("3.66");
});
